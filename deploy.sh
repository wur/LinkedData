#!/bin/bash
#=======================================================================================
#title          :BioSB Course
#description    :Documentation
#author         :Jasper Koehorst
#date           :2023
#version        :0.0.1
#=======================================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Build book
cd $DIR
jupyter-book clean . && jupyter-book build .