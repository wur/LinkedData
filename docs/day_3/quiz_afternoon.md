# Afternoon quiz

In this tutorial we are going to explore the more advanced features of SPARQL. 

## Advanced local queries

***1. Regular expression***
Write a query that uses a regex pattern (regular expressions) to find names meeting a complex criterion.

````{admonition} Reveal the answer
:class: dropdown
```{code}
select * where {
  ?creature dbp:name ?name .
  FILTER ( REGEX(?name, "^[RI].*x$" ) )
}
```
````


***2. Weight selection***
Select subjects described with the tto:weight predicate and order them accordingly.

````{admonition} Reveal the answer
:class: dropdown
```{code}
select ?thing ?weight where {
  ?thing tto:weight ?weight .
  FILTER (?weight > 5 && ?weight < 7.0) 
}order by ?weight

#by default the direction of ORDER BY is ascending (asc), use desc() to use descending direction. 
#using ; between multiple predicate - object pairs  is useful when they are related to a common subject   
#try to use desc(?weight)

#try to filter on strings
# Selects subjects described with the tto:color predicate. ?thing, ?color are the variables, 
# the filter defines 2 criteria. One of them must be met otherwise solutions are removed from the result set
# select ?thing ?weight where {
#  ?thing tto:color ?color .
#    FILTER (?color = "grey" || ?color = "white" )
# }
```
````

***3. Weight selection and filtering***
Select subjects described with the tto:weight predicate that are between 5 and 7 and order them accordingly.

````{admonition} Reveal the answer
:class: dropdown
```{code}
select ?thing ?weight where {
  ?thing tto:weight ?weight .
  FILTER (?weight > 5 && ?weight < 7.0) 
} order by ?weight

# Selects subjects described with the tto:color predicate. ?thing, ?color are the variables, 
# the filter defines 2 criteria. One of them must be met otherwise solutions are removed from the result set
# An example for an OR query using colors
# select ?thing ?weight where {
#  ?thing tto:color ?color .
#    FILTER (?color = "grey" || ?color = "white" )
# }
```
````

***4. How old are they?***
The age is computed using the functions YEAR() and NOW().  The resulting value is assigned to a new variable ?age in the BIND clause.

````{admonition} Reveal the answer
:class: dropdown
```{code}
select * where {
  ?person rdf:type dbo:Person .
  ?person dbp:birthDate ?birth .
  BIND ( ( year(now()) - year(?birth) ) AS ?age )
}
order by desc(?age)
```
````

***5. Gender couting in the dataset***
The graph pattern matching process generates the possible solutions as a list of ?gender value and ?people value pairs
then for each ?gender value (grouping criterion), the number of ?people values is counted (aggregate function COUNT)

````{admonition} Reveal the answer
:class: dropdown
```{code}
select ?sex (COUNT(?people) as ?peopleCount) where {
  ?people rdf:type dbo:Person .
  ?people tto:sex ?sex .
}
GROUP BY ?sex
```
````

## Wikidata - Life sciences
 
https://query.wikidata.org/
 
**Use CTRL + SPACE for autocompletion and help with searching the right properties**
 
wdt: (for predicates unless wanted otherwise)
wd: (subject / object from items)
 
The **SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }**
 
is a handy property to show the labels of ?something when typed as ?somethingLabel
 
**Due to the vast resource and dynamic nature there is not really a map of wikidata connections…**
 
But you can easily browse and search through wikidata pages

There are several topics with example queries and questions. Most of the queries will have "xxxx" that you need to replace by something sensible. 

The topics are:
- [Family relationships](family-relationships)
- [Food (pizza)](food-pizza)
- [Location (geographical) ](location-geo)

Optional/Additional questions:

- [Endangered species](endangered-species)
- [Cities and their location](cities-location)
- [Countries and demography (life expectancy)](life-expectancy)
- [Art and paintings](art-paintings)

Feel free to diverge from this path if you have an interesting topic of your own, as long as you include them in the logbook it is fine. Also if you find an item with missing information and you have a source that you would like to include, let us know!

(family-relationships)=
### Its all about Family

***Who is the king of the Netherlands?***

Hint: You can obtain this information by viewing information about The Netherlands. The key is that you need to find the subject (URI) that represents him. 

***1. Who is his mother?***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?motherLabel WHERE {
    wd:xxxx wdt:P25 ?mother .
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } 
}
```
````

***2. Who is the mother of his mother?***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?motherLabel WHERE {
wd:xxxx wdt:P25/wdt:xxx ?mother .
SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } }
```
````

***3. Who is the mother of the mother of the mother of the mother of … of … of his mother?***

You might want to look into property paths (Page 66 of the Learning SPARQL book), alternative you can check the W3 documentation on property paths and the use cases at: https://www.w3.org/2009/sparql/wiki/TaskForce:PropertyPaths#Use_Cases (maybe the most interesting use case would be one on "Follow a property" )

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?motherLabel WHERE {
 wd:xxxx wdt:xxx ?mother .
 SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
}
```
````

***4. About the mothers… do they have a picture? Can you show them?***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?motherLabel ?image WHERE {
 wd:xxxx wdt:xxxx ?mother .
 xxxxx {?mother wdt:xxxx ?image . }
 SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } 
} ORDER BY ?xxxx
```
````

(food-pizza)=
## Food related topics

There is a lot of information about a large variety of topics including food and who does not like food!? (also we really wanted to include an example on pizza!) 

***How many different pizza's are there in wikidata?***

````{admonition} Give me some help
:class: dropdown
If you go to the wikidata page of pizza: https://www.wikidata.org/wiki/Q177 and click on *What links here* it will show you all pages that links to the object Pizza.

- fried pizza (Q3905962)
- pizza cutter (Q1546715)
- Banchero (Q2882676)

Make sure you click on a pizza and check the page to see what kind of predicate they use to link the subject with Pizza.

```{code}
SELECT ?item ?itemLabel WHERE { 
    ?item wdt:PXXX wd:Q177 .
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". 
    }
}
```
````

***Which ingredient is most frequently used?***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT xxx (COUNT(?xxx) AS ?count) WHERE { ?item wdt:xxx wd:Q177 .
 ?item wdt:xxx ?ingredient .
 SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } } GROUP BY ?xxx
```
````

What do you see if you compare the results of the ingredients with the result of the different pizza's from the previous query? (Optional) Could you maybe help out?

(location-geo)=
## Location queries

***All the things that are located in Wageningen***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?item ?itemLabel
WHERE {
 ?item wdt:xxxx wd:xxxx .
 SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
}
```
````

***The GPS coordinates of all the things that are located in Wageningen***

Hint: When you have the coordinates you can change the view to map

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?item ?itemLabel ?coords
WHERE {
 ?item wdt:xxxx wd:xxxx .  ?item wdt:xxxx ?coords .
 SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
}
```
````

***From before but now that have a picture***

Make sure you place the ?image variable in the SELECT statement...

***From before but now that do NOT have a picture***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?item ?itemLabel ?coords
WHERE {
 ....
 MINUS { ?item wdt:P18 ?image . }
....
}
```
````

***Remove streets***

Many of the items are street items and you do not need a picture of every street right? So lets have a look what is left when you remove the streets?


***What are the coordinates from Wageningen University (Check wikidata.org)***

````{admonition} Give me some help
:class: dropdown
When you go to the wikidata page of Wageningen University you should see the image below. The coordinates of WUR are 51°59'7.0"N, 5°39'49.2"E or Point(5.663666666 51.985277777)

![](../images/wur-coords.png)
````

***Obtain the coordinates using SPARQL***
This is a bit tricky as you are now trying to obtain the information in the qualifiers section. If you would use a normal *wdt* statement you will obtain Wageningen and not the qualifier information. 

To obtain the right information you need to use the ***p,ps,pq*** predicates.

````{admonition} Give me some help
:class: dropdown
```{code}
  wd:Q422208 p:P159 ?wurLoc .
  ?wurLoc pq:P625 ?coord .
```
````

```{admonition} Give me some help
:class: dropdown
When you go to the wikidata page of Wageningen University you should see the image below. The coordinates of WUR are 51°59'7.0"N, 5°39'49.2"E or Point(5.663666666 51.985277777)

![](../images/wur-coords.png)
```

***Everything in 10 kilometers from the university***

This is another service query that you need to use so I give this one for free but do not forget to combine it with the Wageningen University!

````{admonition} Give me some help
:class: dropdown
```{code}
#defaultView:Map
SELECT * # ?place ?placeLabel ?location
WHERE
{
  # WUR Location coordinates  
  wd:Q422208 p:P159 ?wurLoc .
  ?wurLoc pq:P625 ?coord .
  # Use the special service for radius  
  SERVICE wikibase:around {  
    ?place wdt:P625 ?location .
    # The wurLoc variable is the GPS coordinates from before the SERVICE statement  
    bd:serviceParam wikibase:center ?coord .
    # 10 is by default in kilometers  
    bd:serviceParam wikibase:radius "10" .
  }
}
```
````

***Again, remove streets and other non-informative points (e.g. neigherboods)***

The MINUS is located AFTER the SERVICE query as it first needs to obtain the results from the SERVICE*...

````{admonition} Give me some help
:class: dropdown
```{code}
#defaultView:Map
SELECT ?place ?placeLabel ?location ?image
WHERE
{
  # WUR Location coordinates  
  wd:Q422208 p:P159 ?wurLoc .
  ?wurLoc pq:P625 ?coord .
  SERVICE wikibase:around {  
    ?place wdt:P625 ?location .
    # The wurLoc variable is the GPS coordinates from before the SERVICE statement  
    bd:serviceParam wikibase:center ?coord .
    # 10 is by default in kilometers  
    bd:serviceParam wikibase:radius "10" .
  } .
  MINUS { }
  MINUS { }
  ?place wdt:P18 ?image .
}
```
````

***Are there any statues there?***

````{admonition} Give me some help
:class: dropdown
```{code}
 # Is a statue
 FILTER EXISTS { ?place wdt:P31/wdt:P279* wd:Q179700 } .
```
````

(endangered-species)=
### Species

*Are there many endangered species in the world (Count them)?*

Hint: The IUCN conservation status (P141) maintains this information

***The number of animals that are extinct in the wild***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?statusLabel (COUNT(?item) AS ?count)
WHERE {
 ?item wdt:xxxx ?status . 
 SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } 
} group by ?statusLabel
```
````

***How do the extinct species look like?***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?item ?image
WHERE {
 ?item wdt:P141 wd:Q237350 . 
 ?item wdt:P18 ?image .
 SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
} 
```
````

(cities-location)=
## Cities
***Give me all the cities in the world and show it on a map***

*Do you see all cities? If not why not?*

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT * WHERE {
...
}
```
````

***All the cities with more than 1.000.000 inhabitants***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT * WHERE {
    ...
 FILTER(?population > 1000000) 
}
```
````

(life-expectancy)=
## Countries

***How many countries are there?***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?item ?itemLabel WHERE {
    ...
 SERVICE ...
}
```
````

***Which country has the highest life expectancy?***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?country ?countryLabel ?life WHERE {
    ...
 SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
}
```
````

***Which country showed the best growth in life expectancy (2009-01-01 vs 2016-01-01)?***

Hint: Ask for life expectancy twice with each its own filter for date

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?countryLabel ?increase WHERE {  ?country wdt:P2250 ?life .
 ?country wdt:P31 wd:Q6256 .
 ?country p:P2250 ?value1 .
 ?value1 pq:P585 ?time1 . 
 ?value1 ps:P2250 ?exp1 . 
 ?country p:P2250 ?value2 .
    ....
 FILTER(?time1 = xsd:dateTime("2009-01-01T00:00:00Z")) 
 FILTER(?time2 = xsd:dateTime("xxxx"))
 BIND(?exp2 - ?exp1 AS ?increase)
 SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
}
```
````

***Number of people per km2***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?item ?itemLabel ?population ?area ?persqk WHERE {
SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }  ?item wdt:P31 wd:Q6256 .
 ...
 BIND(?xxx / ?xxx AS ?xxx)
}
```
````

***Did the country with the highest density came as a surprise?***

***How high is your country on the ranking?***

(art-paintings)=
## Paintings

***Who made the most paintings in the world as far as we can tell?***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?maker (COUNT(?item) AS ?count) WHERE {  ?item wdt:xxx ?maker .
 ?item wdt:P31 wd:xxxx .
} GROUP BY ?maker
Can you show me the paintings?
SELECT ?maker ?item ?image WHERE {  VALUES ?maker { wd:Q320590 }  ?item wdt:xxx ?maker .
 ?item wdt:P31 wd:xxxx .
 ?item wdt:xxxx ?image .
}
```
````

***Can you find the artist with the most paintings (which has a picture)***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?maker (COUNT(?item) AS ?count) WHERE {
    ...
} GROUP BY ?maker
```
````

***And lets have a look at his paintings...***

Hint: Using values we can specify what ?maker should consist of

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?item ?image WHERE {  VALUES ?maker { wd:xxx }  ?item wdt:P170 ?maker .
 ?item wdt:P31 wd:Q3305213 .
 ?item wdt:P18 ?image .
}
```
````

### Big paintings!

***Which country has the most paintings that are wider than 1 meter? and what about 10 meter?***

````{admonition} Give me some help
:class: dropdown
```{code}
SELECT ?country ?countryLabel (COUNT(?country) AS ?count) WHERE {
 SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
 ...
 FILTER(?width > xxxx)
} GROUP BY ?country ?countryLabel
```
````

## UniProt

Querying UniProt with SPARQL

The SPARQL end point https://sparql.uniprot.org  provides examples on how to query.  Note. There is a mismatch between the prefix used in the figures (for instance the figures indicate that up_core is used for  <http://purl.uniprot.org/core/>   however the prefix added in the UniProt SPARQL end point is given by:  PREFIX up: <http://purl.uniprot.org/core/> 

The following query will return all the TaxonIDs and the species names in UniProt. 

```{code}
PREFIX up:<http://purl.uniprot.org/core/>
SELECT ?taxon ?name
FROM <http://sparql.uniprot.org/taxonomy> 
WHERE{
?taxon a up:Taxon .
?taxon up:scientificName ?name .
}
```

You can use the “FILTER” command to find the taxonID  of  Mycobacterium tuberculosis (strain ATCC 25618 / H37Rv) 

You can get additional information of the link between genes an and proteins and locustags (also called locus names). The following query returns the proteins and locus tags of coding genes on a of a given organism (selected with the taxonomyID).

````{admonition} Give me some help
:class: dropdown
```{code}
PREFIX up:<http://purl.uniprot.org/core/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT ?gene ?locusName
WHERE
{
 ?protein up:organism <http://purl.uniprot.org/taxonomy/83332>.
?protein a up:Protein .
 ?protein up:encodedBy ?gene . 
 ?gene up:locusName ?locusName .
}
```
````

UniProt also contains additional annotation linking to external databases with the rdfs:seeAlso predicates. Write a follow-up query that provides the list of PFAM domains identified in the genome of a given organism.  

````{admonition} Give me some help
:class: dropdown

Here we selected the taxonID of. "Corynebacterium sp. HMSC14B06")

```{code}
PREFIX up:<http://purl.uniprot.org/core/> 
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT ?uniprot ?seeAlso
WHERE 
{
 ?uniprot up:organism <http://purl.uniprot.org/taxonomy/1581098>. 
 ?uniprot a up:Protein .
 ?uniprot rdfs:seeAlso ?seeAlso .
?seeAlso up:database <http://purl.uniprot.org/database/Pfam> . 
}  

```
````

## UniProt and Wikidata

In the following we will combine queries that mine WIKIDATA  and UNIPROT.   In fact we are going to query from the UniProt SPARQL end point https://sparql.uniprot.org 

1. We start with at GO to  https://sparql.uniprot.org  and perform a Wikidata query from the UniProt database (do not forget the prefixes!!)

Retrieve all human proteins that are located in the mitochondria and can bind metal ions.  

````{admonition} Give me some help
:class: dropdown

Note that we are just querying Wikidata, only from UniProt.  
If you check the autocompletion service in Wikidata the queries says:   
wdt:P703  means  “found in taxon” and wd:Q15978631 is “homo sapiens” 
wdt:P681 means “cell component” and wd:Q39572 is “mitochondria” 
wdt: P680 is “molecular function” and wd:Q136677380 means “meatal ion binding” 

```{code}
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
SELECT DISTINCT ?item  
WHERE {
	SERVICE <http://query.wikidata.org/sparql> 
{
?item wdt:P703 wd:Q15978631 .
?item wdt:P681 wd:Q39572 .
?item wdt:P680 wd:Q13667380 .
} 
}LIMIT 10
```
````

2.	To couple the results, we need an exactMatch to get the uniProt URLs.   Adding the last line ensures that we have a common link between databases (although so far it changes nothing). 

````{admonition} Give me some help
:class: dropdown
```{code}
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
SELECT DISTINCT *  WHERE {
	SERVICE <http://query.wikidata.org/sparql> {
?item wdt:P703 wd:Q15978631 .
?item wdt:P681 wd:Q39572 .
?item wdt:P680 wd:Q13667380 .
?item wdt:P2888 ?uniprotURL .
 	}
}LIMIT 10
```
````

3. Now we query both databases at the same time so that we use wikidata to get mitochondrial human proteins that bind iron and then we use UniProt to find if they have an ec number. 
Do not forget the up prefix…. 

````{admonition} Give me some help
:class: dropdown
```{code}

PREFIX up:<http://purl.uniprot.org/core/> 
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
SELECT DISTINCT *  WHERE {
SERVICE <http://query.wikidata.org/sparql> {
	
?item wdt:P703 wd:Q15978631 .
	?item wdt:P681 wd:Q39572 .
	?item wdt:P680 wd:Q13667380 .
?item wdt:P2888 ?uniprotURL .
 	}
?uniprotURL up:enzyme ?enzyme .
}

Here the order is important to get the query running in a reasonable amount of time… 
```
````