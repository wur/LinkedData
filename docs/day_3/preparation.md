# Preparation

After reading the `SPARQL Playground` chapter you should have an idea of the structure of the dataset.
To be able to query the data we use the `RDF data` section shown below. Make sure you click on the top right copy icon (within the box) to ensure that all content is copied to the clipboard.

## Loading the data

By now you should have installed GraphDB on your local machine. If not please have a look at the installation chapter. 
After the installation you should be able to access the application at http://localhost:7200. To load the data into a database do the following:

- Click on repositories
- Create new repository
	- GraphDB Repository
- Give it a name in the Repository ID*
	- Leave everything else as default
- Top right choose repository change to the one you just created
- Import
- Import RDF text snippet
- Paste the content below in the text box
- Make sure Format: Turtle is selected
- Click import
- Leave everything at default and Click import again
- The data should be loaded within a second

When you go back to the home screen (click GraphDB) you should see your local active repostiroy having a total of 189 statements.

## RDF data

```
@prefix dbo: <http://dbpedia.org/ontology/> .
@prefix dbp: <http://dbpedia.org/property/> .
@prefix dbpedia: <http://dbpedia.org/resource/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix tto: <http://example.org/tuto/ontology#> .
@prefix ttr: <http://example.org/tuto/resource#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

dbo:Person rdfs:subClassOf tto:Creature .

tto:Animal a rdfs:Class ;
	rdfs:isDefinedBy <http://example.org/tuto/ontology#> ;
	rdfs:label "animal" ;
	rdfs:subClassOf tto:Creature .

tto:Cat a rdfs:Class ;
	rdfs:isDefinedBy <http://example.org/tuto/ontology#> ;
	rdfs:label "cat" ;
	rdfs:subClassOf tto:Animal .

tto:Creature a rdfs:Class ;
	rdfs:isDefinedBy <http://example.org/tuto/ontology#> ;
	rdfs:label "creature" .

tto:Dog a rdfs:Class ;
	rdfs:isDefinedBy <http://example.org/tuto/ontology#> ;
	rdfs:label "dog" ;
	rdfs:subClassOf tto:Animal .

tto:Monkey a rdfs:Class ;
	rdfs:isDefinedBy <http://example.org/tuto/ontology#> ;
	rdfs:label "monkey" ;
	rdfs:subClassOf tto:Animal .

tto:pet a rdf:Property ;
	rdfs:domain dbo:Person ;
	rdfs:isDefinedBy <http://example.org/tuto/ontology#> ;
	rdfs:label "domestic animal" ;
	rdfs:range tto:Animal .

tto:sex a rdf:Property ;
	rdfs:domain tto:Creature ;
	rdfs:isDefinedBy <http://example.org/tuto/ontology#> ;
	rdfs:label "sex" ;
	rdfs:range xsd:string .

tto:weight a rdf:Property ;
	rdfs:comment "weight in kilograms" ;
	rdfs:domain tto:Creature ;
	rdfs:isDefinedBy <http://example.org/tuto/ontology#> ;
	rdfs:label "weight" ;
	rdfs:range xsd:decimal .

ttr:Eve dbo:parent ttr:William ;
	dbp:birthDate "2006-11-03"^^xsd:date ;
	dbp:name "Eve" ;
	tto:sex "female" ;
	a dbo:Person .

ttr:John dbp:birthDate "1942-02-02"^^xsd:date ;
	dbp:name "John" ;
	tto:pet ttr:LunaCat , ttr:TomCat ;
	tto:sex "male" ;
	a dbo:Person .

ttr:LunaCat dbp:name "Luna" ;
	tto:color "violet" ;
	tto:sex "female" ;
	tto:weight 4.2 ;
	a tto:Cat .

ttr:RexDog dbp:name "Rex" ;
	tto:color "brown" ;
	tto:sex "male" ;
	tto:weight 8.8 ;
	a tto:Dog .

ttr:SnuffMonkey dbp:name "Snuff" ;
	tto:color "golden" ;
	tto:sex "male" ;
	tto:weight 3.6 ;
	a tto:Monkey .

ttr:TomCat dbp:name "Tom" ;
	tto:color "grey" ;
	tto:sex "male" ;
	tto:weight 5.8 ;
	a tto:Cat .

ttr:William dbo:parent ttr:John ;
	dbp:birthDate "1978-07-20"^^xsd:date ;
	dbp:name "William" ;
	tto:pet ttr:RexDog ;
	tto:sex "male" ;
	a dbo:Person .
```