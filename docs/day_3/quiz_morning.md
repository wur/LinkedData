# Morning quiz

## Data exploration

***1. How does the data look like?***
To have an idea of the data contained in a dataset you can ask for a sample. Don't forget to include the LIMIT keyword to avoid problems of resources and memory

````{admonition} Reveal the answer
:class: dropdown
```{code}
SELECT *
WHERE {
    ?subject ?predicate ?object .
}
LIMIT 100
```
````

This way you can view a snapshot of the data (especially when you have millions of statements). If you would like to explore this further you can for example have a look at `ttr:LunaCat` which in my resultset is `Row 53	ttr:LunaCat - rdf:type - tto:Animal`. If you click on LunaCat it brings you to the statement overview of LunaCat.

| **Row** | subject | predicate | object | context |
|---------|--------------|----------------|--------------------|----------------------------------|
| **1**   | ttr:LunaCat  | dbp:name       | "Luna"             | http://www.ontotext.com/explicit |
| **2**   | ttr:LunaCat  | tto:color      | "violet"           | http://www.ontotext.com/explicit |
| **3**   | ttr:LunaCat  | tto:sex        | "female"           | http://www.ontotext.com/explicit |
| **4**   | ttr:LunaCat  | tto:weight     | "4.2"^^xsd:decimal | http://www.ontotext.com/explicit |
| **5**   | ttr:LunaCat  | rdf:type       | tto:Cat            | http://www.ontotext.com/explicit |

To obtain a complete URL you can click on the `chain` or use the Source URL depicted at the top: `Source: http://example.org/tuto/resource#LunaCat`

***Explore Visually***

To explore the datasset visually (make sure you have the URL of interest at hand) do the following:
- Click on Explore (Left menu)
- Click on Visual graph
- Paste the URL in `Search RDF resources`
- Click Show

Now you should see that `LunaCat` is connected to 4 different nodes. 

- LunaCat > type > Cat
- LunaCat < domestic animal < John
- LunaCat > type > creature
- LunaCat > type > animal

If you click on LunaCat you should get an info box on the right with LunaCar information about type, sex, weight, name and color.

- Now click on John

As you can see the graph now expands showing that John is a Person, has another domestic animal and is the parent of William.

![](../images/graphdb-visual-graph.png)
*An example of a visualization with LunaCat and John.*

> Feel free to explore further!

***2. Which classes are declared in the dataset?***

There are 2 common ways of defining classes (using rdfs or owl). You could say a ?class is connected by rdf:type = a to rdfs:Class or or you could say a ?class is a owl:Class. 

````{admonition} Reveal the answer
:class: dropdown
```{code}
SELECT distinct ?class
WHERE {
    ?class a rdfs:Class
    #you may want to replace rdfs:Class with owl:Class
}
order by ?class
```
````

***3. How many triplets are contained in the dataset?***

You can have an idea on how big a dataset is by asking how many triples it contains. Depending on how large the dataset is, this query may take some time (be patient and just wait for an answer)

````{admonition} Reveal the answer
:class: dropdown
```{code}
SELECT (COUNT(*) AS ?no) 
WHERE { ?s ?p ?o  }
```
````

***4. How many instances of a certain class are declared?***

To count the number of instances of a particular class you can use the keyword COUNT with the name of the class (e.g.,  tto:Animal)

````{admonition} Reveal the answer
:class: dropdown
```{code}
SELECT (COUNT(?s) AS ?totalNumberOfInstances)
WHERE { ?s rdfs:subClassOf tto:Animal } 
# replace tto:Animal with the name of another class
```
````

***5. How to get the declared properties?***

Selects the declared properties. Note that the dataset may be using properties that are not "declared" (they may be declared in other dataset)

````{admonition} Reveal the answer
:class: dropdown
```{code}
SELECT ?property
WHERE {
    ?property a rdf:Property . 
}
```
````

***6. What properties are used?***

Includes properties in use in the dataset (even if they are not "declared" in this dataset).

````{admonition} Reveal the answer
:class: dropdown
```{code}
SELECT DISTINCT ?property
WHERE
{ ?s ?property ?o . }
LIMIT 50
```
````

***7. Which classes use a particular property?***

In this question we would like to see which classes uses dbp:name

````{admonition} Reveal the answer
:class: dropdown
```{code}
SELECT DISTINCT ?class
WHERE {
    ?subject dbp:name ?o .
    ?subject a ?class .
}
 # It is a good practice to include rdfs:label, rdfs:comment or 
 # rdfs:domain to describe data
```
````

***8. How often is a property used?***

In this query we would like to see how often the property dbp:name is used.

````{admonition} Reveal the answer
:class: dropdown
```{code}
SELECT (COUNT(?o) AS ?totalNumberOfNames)
WHERE { ?s dbp:name ?o }
```
````


## Questions to query

***9. Select things that are female***

Selects subjects connected to the literal "female" via the predicate tto:sex ?thing is the only variable

````{admonition} Reveal the answer
:class: dropdown
```{code}
select ?thing where {
  ?thing tto:sex "female" .
}

# Notice that not only the persons
# but also the pets are taken
```
````

***10.Select things that are persons and are female (women)***

Selects subjects connected to the object dbo:Person via the predicate rdf:type and use the same variable '?thing' to connect to the literal female by the predicated tto:sex.

````{admonition} Reveal the answer
:class: dropdown
```{code}
select ?thing where {
  ?thing a dbo:Person .
  ?thing tto:sex "female" .
}

# Use the same name of the variable in the 2 statements
# It is the name of the variable that enforces the constraint

# Note the dot "." which must be added in the first statement, otherwise we get a MalformedQueryException

# Hint: Use the semicolon ';' to refer to the previous subject 
#
# select ?thing where {
#  ?thing a dbo:Person ;
#           tto:sex "female" .
#}

# Note that we could also use the comma ',' , if we had hermaphrodites in our dataset
# The following statements selects things that are persons male and females at the same
# select ?thing where {
#  ?thing a dbo:Person ;
#           tto:sex "female" , "male" 
# }
# Of course this does not return any value in our "normal" dataset....
```
````

***11.Select persons and their pets***

From now one let's assume that "select persons" means "select things that are persons" by selecting subjects connected to the object dbo:Person via the predicate rdf:type. In this case we also want the ?person to be connected to an object ?pet via the predicate tto:pet ?person and ?pet are the 2 variables

````{admonition} Reveal the answer
:class: dropdown
```{code}
select ?person ?pet where {
    ?person rdf:type dbo:Person .
    ?person tto:pet ?pet .
}

# another example where we select 2 variables
#
# notice that only the persons 
#
# who actually have a pet are returned in the result set
```
````

***12. Select persons and, if they have, their pets as well***

Similar to the previous one, but putting the
the triple linked with tto:pet inside the optional clause

````{admonition} Reveal the answer
:class: dropdown
```{code}
select ?person ?pet where {
    ?person rdf:type dbo:Person .
    optional {?person tto:pet ?pet }.
}

# The use of the clause optional allows
# to extract their pets if they exist
# but will not exclude the persons who don't have pets
```
````

***13. Select persons that DO NOT have any pets***

Selects persons and assures that the selected persons do not contain any link to an object via the predicate tto:pet.

````{admonition} Reveal the answer
:class: dropdown
```{code}
select ?person ?pet where {
    ?person rdf:type dbo:Person .
    filter not exists {?person tto:pet ?pet }.
}

# Note that the variable ?pet is not bound even if you use filter exists
# filter exists {?person tto:pet ?_ }.
```
````

***14. William's and John's pets***

Selects the pets of a list of owners using the clause union or values

````{admonition} Reveal the answer
:class: dropdown
```{code}
select ?pet where {    
    { 
        ttr:William tto:pet ?pet .
    } UNION {
        ttr:John tto:pet ?pet .
    }
}

# In this scenario we only have 2 persons who have pets, so it is not the best example
# but you could see the potential of union in a real scenario, where we would have many owners and we would 
# like to filter just a few of them
#
# Alternatively you can also use the keyword VALUES that sets what the values of ?owner could be (faster option)
#
# select ?owner ?pet where {
#    VALUES (?owner) { (ttr:William) (ttr:John) }
#    ?owner tto:pet ?pet .
#}
```
````

***15. Select Eve's grandfather***

Use the property dbo:parent to connect Eve at his father ...

Explore the use of slash / (Path queries)

````{admonition} Reveal the answer
:class: dropdown
```{code}
select ?grandfather where {
    ttr:Eve dbo:parent  *** .
    ***               ***   ?grandfather  .
}

# Alternative B) with /
#
# Once you get ttr:John,
# try to write the expression in only one line using '/'
# knowing that:
#
#             ?a prop ?c .
#              ?c prop ?d .
#
# can be simplified like this:
#
#             ?a prop / prop ?d
```
````

***16. Select things that are persons***

Selects subjects connected to the object dbo:Person via the predicate rdf:type ?thing is the only variable

````{admonition} Reveal the answer
:class: dropdown
```{code}
select ?thing where {
  ?thing rdf:type dbo:Person .
}

# Remember that in the turtle data (Data tab) we can see: 
#
# 'ttr:John a dbo:Person'
#
# 'a' can also be used instead of 'rdf:type' 
# 'a' is a synonym of 'rdf:type'

# The name of the variable can have any value

# Explore snorql:
#
# Click on ttr:William to see the properties attached to it
```
````