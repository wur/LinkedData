---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Birds! 

This exercise is designed to make you familiar with the OntoRefine application of Ontotext. This module will allow you to transform datasets into an RDF dataset.. 

In this exercise you will learn how to upload a CSV dataset containing observations of the `Limosa lapponica` species that was obtained from iNaturalist and convert it to RDF. 

![](../images/1080px-Bar-tailed_Godwit.jpg)
*The Bar-tailed Godwit*

## Converting data 

### Example data 

For the examples in this guide, we will be working with the following dataset: 

- [Observations of the Bar-tailed Godwit](https://gitlab.com/wur/linkeddata/-/raw/main/material/observations-301836.csv.gz)

As you can see this is a compressed (gzip) file you can use the compressed file in the exercise.

### UML Diagram 

It is important that you understand the data. In this case it is a compressed CSV file. You can upload it already into OntorRefine (next section) or open (after decompression) in any text editor (e.g. sublime, visual studio code, excel) to see what the headers and content look like. 

When you had a look at the data you could design an UML diagram in which you can specify the relationships and the statement types. For example a limited UML diagram on the dataset could look like this: 

![](../images/uml-diagram-inaturalist.png)
  
*Figure 1: UML Diagram example with class and predicate definitions mapped to (non-)existing ontologies.*
 
It is up to you on how many classes you will create. Will there be any subclass of relationships for certain class types and where you put all the properties.

### Ontologies 

In this example we will use an already existing ontology or at least parts of it. This ontology (schema.org) contains many of the elements that are needed in this tutorial. When working with datasets of which ontologies only partially fit your data it might be important to develop your own ontology. This can be a combination of existing ontology class type and predicate URL’s with your own self-defined URLs. 

This works best using Protege, there is a web version with limited capabilities and an offline version that you can run on your computer. 

Using the UML diagram you can more easily create your OWL class representations and property types. 

***Obtaining schema.org***

At the website of schema.org you can download the properties in various different formats. The format that we will use is written in RDF/XML using the OWL specifications.

https://schema.org/docs/schemaorg.owl

You can open this file in protege to explore all classess and properties that have been developed in schema.org.

You can load this schema directly into your graphdb instance using the URL provided above. 

In addition you can create your own ontology, preferably by recycling terms from already existing ontologies using Protegé (See section from day 2).
 
### GraphDB

-	Start GraphDB in Workbench mode. 
-	Open http://localhost:7200/ in a browser. 
-	Setup > Repositories 
-	Create new repository 
-	Fill in the Repository ID 
-	Optionally, change the Base URL to something more fitting 
  - This web adres does not have to exists (e.g. http://observations.nl/) 
-	Click create 
-	Make sure your repository is connected (powerplug icon in front of the repository name) 
-	Enable Autocomplete (Under Setup) 

**Loading the ontology**

If you have created an ontology please load this file:

-	Load the ontology you created
-	The ontology should be loaded (Simple query should contain your classes and objectProperties)

![](../images/graphdb-ontology.png)

Otherwise...
- Obtain the ontology from https://schema.org/docs/schemaorg.owl
- You can load this directly from the URL

![](../images/graphdb-schema-org.png)



### OntoRefine – overview and features 

GraphDB OntoRefine is an upgraded version of the open source OpenRefine data transformation tool. It allows the quick mapping of any structured data to a locally stored RDF schema in GraphDB. The visual interface is optimized to guide you in choosing the right predicates and types, defining the datatype to RDF mappings, and implementing complex transformation using OpenRefine’s GREL language. GREL is the 
Google Refine Expression Language that helps you define complex transformation. OntoRefine is integrated in the GraphDB Workbench, and supports the formats TSV, CSV, *SV, XLS, XLSX, JSON, XML, RDF as XML, and Google sheet. It enables you to: 

- Upload your data file(s) and create a project. 
- Create an RDF model of the cleaned data. 
- Transform your data using SPIN functions. 
- Further modify your RDFized data in the GraphDB SPARQL endpoint. 

***Start Ontotext-refine (https://www.ontotext.com/products/ontotext-refine/)***

To access your ontology data, you need a working and running GraphDB database on the same machine (easiest). 

-	In the ontorefine web page of your local machine go to setup
    - Change the repository id to your repo name
    - Save configuration 

This should make the connection between your graphdb instance and the ontorefine instance.


### Create a project 

All data files in OntoRefine are organized as projects. One project can have more than one data file. 
The Create Project action area consists of three tabs corresponding to the source of data. You can upload a file from your computer, specify the URL of a publicly accessible data, or paste data from the clipboard. 

1.	Go to Import > Tabular (OntoRefine). 3. Click Create Project > Get data from. 
2.	Select the file to upload: 
  - from your computer 
    - Select your csv file
  - or from a URL 
    - Provide the URL to obtain the file 
3.	Click Next 
  
![](../images/graphdb-upload.png)
*The upload screen*

![](../images/graphdb-ontorefine-table.png)
*The table overview*

When you are satisfied with the ingestion structure click `Create Project`.

### Exploring the dataset 

As shown in the figure you should now be able to see the formatted table with the content from the file that was uploaded. To, for example, explore the different cities in this dataset you can setup a facet viewer

- click on the downwards arrow next to the `place town name` column 
- Facet > Text Facet

![](../images/graphdb-facet.png)
  
As you can see, in this dataset there are among others, 9 observations from Airlie Beach and 4542 without information. You can click on any of the values to filter further. 

### Cleaning the project table 

***Fixing UTC timestamps***

As can be seen in the facet viewer of the `time_observed_at` names, all timepoints have a UTC extension which makes it difficult to transform the column to date time. 
  - click on the downwards arrow next to the time_observed_at column 
  - Edit cells 
  - Replace
  - Replace " UTC" by nothing make sure you add a space before or trim spaces afterwards
  - click again on the downwards arrow next to the time_observed_at column 
  - Edit cells
  - Common transforms
  - To date

Now all values are transformed into a date time stamp.

![](../images/graphdb-datetime.png)

### Reconciliation

As you have learned in the SPARQL exercises there are other resources that you could link your data with. This method is called reconciliation and it uses your input data to map against known entities in WikiData. 

For example, the taxonomic information of the bird is known in wikidata, locations of its observations recorded at city, county or country level are mostly known as well. 

To achieve reconciliation try the following:

- Go to the column `place_country_name`
- Click on the down-wards pointing arrow
- Reconcile
- Start reconciling

A new screen should pop up

- Select Locations (Wikidata)

Based ont he column information it should automagically select `country`, 
http://www.wikidata.org/entity/Q6256. You can select relevent other fields which is mostly usefull if you are looking at streets, cities, etc for which country or other more generic information might be usefull.

- Start Reconciling

This should take less than a minute to complete.

The column values should now be highlighted blue and if you hover over it you should see to what entity it matched.

![](../images/ontotext-refine-reconciliation-uk.png)

It is very usefull and it would make life a bit easier if you add the reconciliation values as a new column to the dataset.

- Click on the menu of the column you reconciled
- Click reconcile
- Add entity identifiers column...
- Give it a name for example: `place_country_name_wd`

A new column should appear with the given name and the wikidata URLs.

### Mapping interface

When you are satisfied with the table formatting we can start the RDF Mapping. 

- Click RDF Mapping
- Edit RDF mapping 

```{warning}
Make sure you use the save button a lot!
If you create a statement, hit save! If the statement is incorrect or invalid it will not allow you to save untill you fix this...

*it's a feature not a bug*
```

The RDF Mapping button will take you to the mapping editor, which is an extension of the OntoRefine function ality. Here, you can: 

- configure and preview the mapping model of your data • save your mapping 
- download a JSON model of your mapping 
- upload a JSON model of a mapping 
- convert the result of the mapping to RDF data that is downloaded as a .ttl file 
- generate a SPARQL query of your mapping and open it in a 
- create a new mapping 

You can close the mapping editor with the X button on the top right.

The headers of all columns in the tabular data that we imported are displayed as boxes that you can drag and drop into a mapping cell to configure them. 

Each row in the table represents an RDF triple constructed from the tabular data. It can have one or more subjects as roots, with each subject having one or more predicates as child elements, and each predicate having one or more objects as child elements. 

![](../images/graphdb-ontorefine.png)

#### Prefixes 

The mapping tree contains a set of prefixes that are used in the cell configuration. They are defined in the prefix area and can be of three types:
- default prefixes from commonly used RDF schemas, such as foaf, geo, rdf, rdfs, skos, xsd. You can select entities from these schemas without importing them in your repository. 
- prefixes that you select from the imported ontology, 
- and such that you create ourself. 
Add the following prefix in the 

***Prefix field:***
```
prefix obs: <http://observation.nl/ontology/>
```

#### Triple configuration 

> You may find it convenient to be able to preview the end RDF results while still being able to configure them. To do so, choose Both from the options on the top left.

#### Observation ID and type 

For the RDF subject value, we will take the value of id column. You do not need to know all the headers of your data, as they are displayed at the top and typing @ in the field will display a dropdown list of all available column headers. 

Set the rdf:type for the predicate - a. 

As object value, enter Observation as present in your ontology. 

![](../images/graphdb-ontorefine-schema.png)

Now let’s edit the subject value. The edit icon will open the Subject mapping configuration window. We can see that the Source for this value is the value of the `id` column. In the Prefix field, type `obs`, which will extend the predefined observation prefix. 

![](../images/graphdb-ontorefine-subject.png)
  
*Which results into*
  
![](../images/graphdb-ontorefine-triple.png)

Also note that the `id` button in the top row is colored grey indicating that it has now been used. 

To view the RDF results at any time, click the RDF button. This will download a result-triples.ttl file where we can see the ...

```
@base <http://example.com/base/> .
@prefix obs: <http://observation.nl/ontology/> .
@prefix schema: <https://schema.org/> .
```
namespace that we defined, as well as the IDs of the observations that come from the values in the id column. 

```{code}
@base <http://example.com/base/> .
@prefix obs: <http://observation.nl/ontology/> .
@prefix : <http://www.semanticweb.org/jasperk/ontologies/2023/2/untitled-ontology-66#> .
@prefix schema: <https://schema.org/> .

obs:52627 a schema:Observation .
obs:56458 a schema:Observation .
obs:56460 a schema:Observation .
obs:56462 a schema:Observation .
...
```

```{warning}
Make sure you use the save button a lot!
If you go to any other menu such as Setup, your progress will be lost if save was not used.

*it's a feature not a bug*
```

As our goal here is to map the tabular data against the schema-org ontology, we will continue adding predicates and objects. Since the mapping table is modeled after the Turtle syntax, we can attach more than one predicate to one subject and more than one object to one predicate without having to repeat the same triples in every row. 

#### XSD

In the next row, let’s add the ***observed on*** value. The predicate we can use is `observationDate` from schema.org by autocompleting it from the imported ontology.

For the value:

As explained above, type @ and select observed_on from the invoked list. As you can see it auomtatically becomes a literal with as value `"2012-02-15T00:00:00Z"`. This is currently registered as a standard string and unfortunately has nothing to do with a date time notation. To fix this 

- click on the pencil.
- Datatype
- constant
- Prefix = xsd
- Constant = dateTime

![](../images/graphdb-ontorefine-xsd.png)

```{note}
XSD (XML Schema Definition) is a language for defining XML document structures, data types, and constraints. In RDF (Resource Description Framework), XSD can be used to define the data types of literals in RDF triples. Here are some of the XSD options in RDF:

1. xsd:string: This is the default data type in RDF, and is used for plain text strings.
2. xsd:boolean: This data type is used for boolean values, which can be either "true" or "false".
3. xsd:integer: This data type is used for integer values, which are whole numbers with no decimal places.
4. xsd:decimal: This data type is used for decimal values, which can have a decimal point and can be either positive or negative.
5. xsd:float: This data type is used for floating-point values, which are numbers with a decimal point that can be expressed in scientific notation.
6. xsd:date: This data type is used for dates, expressed in the format YYYY-MM-DD.
7. xsd:dateTime: This data type is used for timestamps, expressed in the format YYYY-MM-DDTHH:MM:SS.
```

#### Taxonomy

In the next row, let’s add another predicate from the ontology – schema:parentTaxon. As its object, we will use the value from the `taxon_species_name` column. We can reuse parent taxon to link the species, genus, family, order class, subphylum, phylum, kingdom which could be usefull when working with observations of different animals.

When you use the column directly you will see that there is no reconciliation with wikidata.

```{note}
If you did not ronciliate this column make sure you have saved your last valid transformation and click on the tiny cross in the top right. You should see the table overview now. Go ahead and reconiliate this column.
```

The easiest method is go to the table overview (click the small cross top right)

- Click on the menu of the column you reconciled
- Click reconcile
- Add entity identifiers column...
- Give it a name for example: `place_country_name_wd`

A new column should appear with the given name and the wikidata URLs.

```{note}
We have seen that sometimes it only shows the Q number instead of the full URL such as http://www.wikidata.org/entity/Q1041.

When this happens replace the Q with http://www.wikidata.org/entity/Q and all paths will become URLs.
```

![](../images/graphdb-ontorefine-reconcile.png)


When you go back to the RDF mapping you should have another box in your overview list with the name you provided. This should contain the URL that you want.

- Add the reconciled URL as an object
- Check if it is a URL if it is a string
  - Edit value
  - Click on IRI
  - Column: Raw IRI
  - Ok
  - The URL should show something like `<http://www.wikidata.org/entity/Q18864>` AND NOT `<http%3A//www.wikidata.org/entity/Q18864>`
- Click on the arrow next to the RAW taxon URL (Add nested triple)
- Now you should be able to see a green box
- In here you can add additional information about the url
- Parent taxon, name, taxon id?

Try it!

An end result could look like:

![](../images/graphdb-ontorefine-taxon.png)

On which you can build upon to set the entire lineage...

If you download the .ttl file with the RDF results and open it, you will see that the data is now mapped the way we intended, e.g.: 

```{code}
obs:52627 a schema:Observation;
  schema:observationDate "2012-02-15T00:00:00Z"^^xsd:dateTime;
  schema:parentTaxon <http://www.wikidata.org/entity/Q18864> .

<http://www.wikidata.org/entity/Q18864> schema:taxonRank "Species";
  rdfs:label "Limosa lapponica";
  schema:parentTaxon <http://www.wikidata.org/entity/Q585636> . 
```

#### Geographical point (as a nested triple) 

Another thing we can map is a geographical point for the observation. To do this, we will define a GeoSPARQL point constructed by the latitude and longitude. For predicate, select geo:hasGeometry by autocompleting it from the preimported GeoSPARQL schema.

For the object, we will need a bridging IRI that will be the GeoSPARQL point. We will take it from the id column: Type > IRI, Source > Column with value id. In the Prefix field, enter your preferred prefix which will extend the predefined prefix.

When something is an IRI, we can continue with the mapping by attaching more predicates and subjects to it, so that it becomes the subject of the next triples. We call these nested triples. You can add one by clicking the Add nested triple (>) icon outside the right border of the object cell. This will open new predicate and object cells below, which are in a green frame together with the ones on top, thus indicating the nesting. 

For this IRI that is now the subject of the nested triple, add in the next row a as predicate type, and sf:Point as object (Source > Constant, and Point as its value). Then click > again to add another nested triple, which will be the point that we will construct. As its predicate, enter geo:asWKT (Source > Constant, then prefix geo and constant asWKT. For the object, we will use the row_index: 

*Hint: Besides using the values from the columns, we can also use the row index, for example: if we did not have a column for the ID in our data (id), we could use the row index to construct an IRI.*

Let’s edit this point. In the object field, set the RDF type to Literal and chose GREL as a source. Type the following GREL expression: 

```
"POINT (" + cells["Longitude"].value.replace(',', '.') + " " + cells["Latitude"].value.replace(',', '.')  + ")".
```

It specifies how the value for Point will be constructed. Note that we combine the values of two columns from our table data here – Latitude and Longitude, to construct one single GeoSPARQL point from them. While typing a GREL expression, a preview of the results is shown to guide you. Click the info icon to reach the GREL documentation. 

Further, for Literal attributes we will set Datatype to configure the Datatype for this Literal. In the Constant field, add prefix geo and then constant wktLiteral to add the geo:wktLiteral Datatype to our Point. 

![](../images/graphdb-ontorefine-latlong-mapping.png)
  
This concludes our example of several basic parameters that can often be used when RDFizing structured data. 

<!-- #### Additional properties

You are free to add additional properties to graph but make sure you extent the appropiate subject as there is one subject related to the `id` used for each observation and there is one subject related to the obs:coords/geometry/@id in which the geographical coordinates are stored. 
You can see how they are linked if it contains 3 column boxes. 
  
![](../images/graphdb-ontorefine-overview.png)

In this overview the schema: the a sf:Point and geo:asWKT are a extension of the object of geo:asWKT. The ***schema:containedInPlace*** with as value ``Amsterdam''@nl is linked to the same subject as geo:hasGeometry and the properties above that. 

***Final overview***

![](../images/graphdb-ontorefine-complete.jpg) -->
  
#### Generating the data 

***Downloading***

Now that the data is restructured according to the OntoRefine configuration it can now generate the RDF data.

- Click the RDF button 

This should prompt a download of an RDF file in the TURTLE syntax. You can view this file in any text file editor such as sublime or notepad++. This document can easily contain thousands of lines but it often helps to briefly view one of the classes to estimate if what you expect is what you have generated.  

#### RDFize data using SPARQL 

If you are a more proficient SPARQL user and want to configure your data in a different way, OntoRefine provides that option as well. The SPARQL Query Editor will allow you to load the RDF mapper content into a `CONSTRUCT` query.

For example

```{code}
BASE <http://example.com/base/>
PREFIX mapper: <http://www.ontotext.com/mapper/>
PREFIX schema: <https://schema.org/>
PREFIX obs: <http://observation.nl/ontology/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
  ?s1 a schema:Observation ;
    schema:observationDate ?o_observationDate ;
    schema:parentTaxon ?o_parentTaxon .
  ?o_parentTaxon schema:taxonRank "Species" ;
    rdfs:label ?o_label ;
    schema:parentTaxon ?o_parentTaxon_2 .
  ?s1 schema:geo ?o_geo .
} WHERE {
  BIND(IRI(mapper:encode_iri(obs:, ?c_id)) as ?s1)
  BIND(STRDT(?c_observed_on, xsd:dateTime) as ?o_observationDate)
  BIND(IRI(?c_taxon_species_name_wd) as ?o_parentTaxon)
  BIND(STR(?c_taxon_species_name) as ?o_label)
  BIND(IRI(?c_taxon_genus_name_wd) as ?o_parentTaxon_2)
  BIND(IRI(mapper:encode_iri(obs:\/geo\/, ?c_id)) as ?o_geo)
}
```

You can also download the mapping SPARQL query results in various formats, including JSON, Turtle, and TriG. 


#### Querying your data resource 

When you have downloaded the RDF data either via de OpenRefine RDF button or via the CONSTRUCT query / Download as (TURTLE) it is now time to permanently load the RDF data into your repository.

- Import > RDF 
- Upload RDF files 
- Click Import 
- You can leave the settings to default 
- Click Import 
- Importing should start now and shows a message when finished (Imported successfully in 16s) 
- Click on SPARQL 
- When needed click on the + icon to have a new tab for your query 
- Execute the following query: 
 
 ```
 PREFIX schema: <https://schema.org/>
select * where { 
	?s a schema:Observation .
}
 ```

- When writing schema: (CTRL + SPACE)
  - autocomplete needs to be enabled for this repository 
- This should show a dropdown menu with properties and types in this ontology 
- You should get 4630 different entries
- Click on one of the URLS 
- You should now have an overview of the properties of this URL 
- When cliking on a link it will browse through the graph internally 
- You can click on the chain when hovering over an URL to copy it to a clipboard 

#### GeoSPARQL support 

***What is GeoSPARQL***

GeoSPARQL is a standard for representing and querying geospatial linked data for the Semantic Web from the Open Geospatial Consortium (OGC). The standard provides: 
- a small topological ontology in RDFS/OWL for representation using Geography Markup Language (GML) and Well-Known Text (WKT) literals; 
- Simple Features, RCC8, and Egenhofer topological relationship vocabularies and ontologies for qualitative reasoning; 
- A SPARQL query interface using a set of topological SPARQL extension functions for quantitative reasoning. 

The GraphDB GeoSPARQL plugin allows the conversion of Well-Known Text from different coordinate reference systems (CRS) into the CRS84 format, which is the default CRS according to the Open Geospatial Consortium (OGC). You can input data of all known CRS types - it will be properly indexed by the plugin, and you will also be able to query it in both the default CRS84 format and in the format in which it was imported. 
The following is a simplified diagram of the GeoSPARQL classes Feature and Geometry, as well as some of their properties: 

![](../images/graphdb-sparql-geo.png)

***Enable plugin***

When the plugin is enabled, it indexes all existing GeoSPARQL data in the repository and automatically reindexes any updates. 

```
PREFIX geoSparql: <http://www.ontotext.com/plugins/geosparql#>
INSERT DATA { [] geoSparql:enabled "true" . }
```

**Distance query**

An example query of two geographical objects for which the distance is calculated.

```{code}
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>
PREFIX ex: <http://example.org/>
PREFIX ext: <http://rdf.useekm.com/ext#>
PREFIX qudt: <http://qudt.org/schema/qudt/>
SELECT *
WHERE {
  ?lmA a ex:Landmark ;
       geo:hasGeometry [ geo:asWKT ?coord1 ].
  ?lmB a ex:Landmark ;
       geo:hasGeometry [ geo:asWKT ?coord2 ].
  BIND((geof:distance(?coord1, ?coord2, uom:metre)) as ?dist) .
  FILTER (str(?lmA) < str(?lmB))
}
```

#### Federated query 
With the dataset now created it is possible to ask other databases if they have more information about the item. It is advised to try to match URI’s from your database with an external database as such elements can be directly matched. 

```{note}
Remember, we achieved this using the reconciliation method with cities, countries and taxonomy...
```

To obtain the label in all languages of the species taxon you could use the following query:

```
PREFIX schema: <https://schema.org/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
select ?obs ?parent ?label where { 
	?obs schema:parentTaxon ?parent .
	?parent schema:taxonRank "Species" .
    SERVICE <https://query.wikidata.org/sparql> {
        ?parent rdfs:label ?label .
    }
} limit 100 
```

Be aware that you might have used different predicates. Make sure you change your query accordingly...

| Row | obs       | parent                                | label                            |
|-----|-----------|---------------------------------------|----------------------------------|
| 1   | obs:52627 | http://www.wikidata.org/entity/Q18864 | "Куралай"@sah                    |
| 2   | obs:52627 | http://www.wikidata.org/entity/Q18864 | "Bandstertgriet"@af              |
| 3   | obs:52627 | http://www.wikidata.org/entity/Q18864 | "Limosa lapponica"@an            |
| 4   | obs:52627 | http://www.wikidata.org/entity/Q18864 | "بقويقة سلطانية مخططة الذيل"@ar  |
| 5   | obs:52627 | http://www.wikidata.org/entity/Q18864 | "بقويقه سلطانيه مخططة الذيل"@arz |
| 6   | obs:52627 | http://www.wikidata.org/entity/Q18864 | "Limosa lapponica"@ast           |
| 7   | obs:52627 | http://www.wikidata.org/entity/Q18864 | "Kiçik oxcüllüt"@az              |
| 8   | obs:52627 | http://www.wikidata.org/entity/Q18864 | "Грыцук малы"@be                 |