---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Programming!

- [SPARQL in Python](http://sparqlwrapper.readthedocs.io): SPARQLWrapper, SPARQL Endpoint interface to Python.
- [RDF/SPARQL in Python](https://rdflib.readthedocs.io/en/stable/intro_to_sparql.html): RDFLib is a pure Python package for working, creating and querying RDF.
- [SPARQL in Java](https://jena.apache.org/tutorials/sparql.html): Jena ARQ - Apache Jena is a Java framework for building Semantic Web and Linked Data applications. Its ARQ module provides a SPARQL engine for executing SPARQL queries.
- [SPARQL in JavaScript](https://github.com/antoniogarrote/rdfstore-js/wiki): rdfstore-js - rdfstore-js is a JavaScript library for working with RDF data. 
- [SPARQL in Ruby](https://rubygems.org/gems/rdf): RDF.rb - RDF.rb is a Ruby library for working with RDF data. 
[SPARQL in PHP](https://www.easyrdf.org): EasyRdf - EasyRdf is a PHP library for working with RDF data.
- [SPARQL in R: SPARQL]: You will see that it was removed from the CRAN repository so it can no longer be installed... (There is a solution!)

## Python SPARQLWrapper

You can install SPARQLWrapper from PyPI:

> pip install sparqlwrapper

### SPARQL

```{code-cell} ipython3
:tags: [remove-stderr]

from SPARQLWrapper import SPARQLWrapper, JSON
import pandas as pd

sparql = SPARQLWrapper("https://query.wikidata.org/sparql")

sparql.setQuery("""
  #Last 10 human genes updated this week
  SELECT DISTINCT ?item ?ncbi_gene ?date_modified
  WHERE {
    ?item wdt:P351 ?ncbi_gene ;
            wdt:P703 wd:Q15978631 ;
            schema:dateModified ?date_modified .
      BIND (now() - ?date_modified as ?date_range)
      FILTER (?date_range < 8)
  } ORDER BY ?date_modified
  LIMIT 10
""")
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

results_df = pd.io.json.json_normalize(results['results']['bindings'])
results_df[['item.value', 'ncbi_gene.value', 'date_modified.value']].head()
```

## Python RDFLib Query and Generation

```{code-cell} ipython3
from rdflib import Graph, URIRef, Literal, BNode
from rdflib.namespace import FOAF, RDF

# Create an empty graph
g = Graph()

# Bind the "foaf" prefix to the FOAF namespace
g.bind("foaf", FOAF)

# Create a URI for Bob's URI
bob = URIRef("http://example.org/people/Bob")

# Create a Blank node for linda
linda = BNode()  # a GUID is generated

# Create literals for bob
name = Literal("Bob")
age = Literal(24)

# Add statements to the graph
g.add((bob, RDF.type, FOAF.Person))
g.add((bob, FOAF.name, name))
g.add((bob, FOAF.age, age))
g.add((bob, FOAF.knows, linda))
g.add((linda, RDF.type, FOAF.Person))

# Example of a self-defined URI
g.add((linda, URIRef("http://example.com/name"), Literal("Linda")))

# Serialze object to default turtle format
print(g.serialize())
```

## R SPARQL

A simple query without the SPARQL library (since it is no longer maintained).

```{code}
library(httr)
library(jsonlite)

# Set the SPARQL endpoint URL
endpoint <- "https://query.wikidata.org/sparql"

# Construct the SPARQL query
query <- "
  SELECT DISTINCT ?item ?ncbi_gene ?date_modified
  WHERE {
    ?item wdt:P351 ?ncbi_gene ;
            wdt:P703 wd:Q15978631 ;
            schema:dateModified ?date_modified .
      BIND (now() - ?date_modified as ?date_range)
      FILTER (?date_range < 8)
  } ORDER BY ?date_modified
  LIMIT 10"

# Encode the query for use in a URL
query_encoded <- URLencode(query)

# Build the URL for the SPARQL endpoint with the encoded query in JSON format
url <- paste(endpoint, "?query=", query_encoded, "&format=", "json", sep = "")

# Send the HTTP GET request to the SPARQL endpoint
response <- GET(url)

# Convert the response to a character string
response_text <- content(response, as = "text")

# Read as JSON
content = fromJSON(response_text)$results$bindings

# Format content
cbind(content$item$value, content$ncbi_gene$value, content$date_modified$value)
```

| Row  | URL | NCBI Gene  | Date |
|----|------------------------------------------|-------|----------------------|
| 1  | http://www.wikidata.org/entity/Q17861998 | 1154  | 2023-02-26T19:40:12Z |
| 2  | http://www.wikidata.org/entity/Q18041474 | 55013 | 2023-02-27T00:40:38Z |
| 3  | http://www.wikidata.org/entity/Q14911585 | 7048  | 2023-02-27T04:51:02Z |
| 4  | http://www.wikidata.org/entity/Q18040930 | 54102 | 2023-02-27T06:24:48Z |
| 5  | http://www.wikidata.org/entity/Q18027367 | 3320  | 2023-02-27T09:15:49Z |
| 6  | http://www.wikidata.org/entity/Q18027700 | 3494  | 2023-02-27T11:16:55Z |
| 7  | http://www.wikidata.org/entity/Q14864369 | 4852  | 2023-02-27T11:28:20Z |
| 8  | http://www.wikidata.org/entity/Q18026980 | 3134  | 2023-02-27T11:33:34Z |
| 9  | http://www.wikidata.org/entity/Q5324714  | 2133  | 2023-02-27T11:54:18Z |
| 10 | http://www.wikidata.org/entity/Q14866071 | 1815  | 2023-02-27T15:34:21Z |