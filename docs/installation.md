---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Installations instructions

*For docker images see below*

## Ontotext - GraphDB standalone

First, go to https://www.ontotext.com/products/graphdb/graphdb-free/ and register for an installation (registration is free and required!). We are particularly interested in version 9.8 however we appreciate you may be downloading later versions. All the practicals should still work with newer versions. For further installation instructions read on or go to http://graphdb.ontotext.com/documentation/free/quick-start-guide.html. 

### Windows
Once you have downloaded the .exe file, open the application file and follow on-screen prompts. When this has completed, locate the application and start the database. You will find that a webpage opens up with webaddress http://localhost:7200/

### Mac
Download the .dmg file and double-click it. This will open a virtual disk with the application the needs to be copied to your Applications folder. Start the database, you will be redirected to a page in your web browser with address http://localhost:7200/

### Linux
Download the .rpm or .deb file and install using sudo rpm -i or sudo dpkg -i followed by the application name in a terminal window.
Start the database by clicking the icon, you will be redirected to your web browser with webaddress http://localhost:7200/

### Docker

> docker run -p 127.0.0.1:7200:7200 --name graphdb-instance -t ontotext/graphdb:10.2.0

You can now access the application at http://localhost:7200/

***Be aware...*** that with this command everything will be erased when you shutdown docker or your computer.
To ensure that any progress made with this graphdb instance you can use the following command to mount a volume on your computer.

> docker run -p 127.0.0.1:7200:7200 -v /my/own/graphdb-home:/opt/graphdb/home --name graphdb-instance -t ontotext/graphdb:10.2.0

Change `/my/own/graphdb-home` to a path of your liking.

In addition when in the future new versions of graphdb are released you can check the appropiate tag at https://hub.docker.com/r/ontotext/graphdb/tags

## Ontotext - Refine standalone

Ontotext Refine ("Refine") is a version of the open-source OpenRefine data transformation tool adapted to work with Ontotext GraphDB. Refine allows fast cleaning, mapping and transformation of any structured data to RDF and loading it to GraphDB.

Refine supports input from:

- Tabular formats (TSV, CSV, *SV)
- Fixed-width text files
- Excel (XLS, XLSX)
- JSON, JSON-LD, XML
- RDF: XML, Turtle/N3
- Google sheets (public or access controlled with OAuth)
- Databases (PostgreSQL, MySQL, MariaDB, SQLite)

### Installation

Similar to the installation of GraphDB different packages are available at https://www.ontotext.com/products/ontotext-refine/download/. Depending on your operating system download the appropiate package or use the platform independent distribution package.

- Platform independent distribution package
- Native apps
  - Windows
  - Mac
  - Linux .deb
  - Linux .rpm
- Docker Images
  - https://hub.docker.com/r/ontotext/refine/tags
  - ontotext/refine:1.2.0

### Docker

docker run --name refine -p 7333:7333 ontotext/refine


## Protege

Protege is a free, open-source ontology editor and framework for building intelligent systems and can be obtained from https://protege.stanford.edu. 
