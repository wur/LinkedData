# Slides

- [Day 1 Morning slides](https://gitlab.com/wur/linkeddata/-/raw/main/material/day1_morning.pdf)
- [Day 1 Afternoon slides](https://gitlab.com/wur/linkeddata/-/raw/main/material/day1_afternoon.pdf)
- [Day 2 Morning slides](https://gitlab.com/wur/linkeddata/-/raw/main/material/day2_morning.pdf)
- [Day 2 Afternoon slides](https://gitlab.com/wur/linkeddata/-/raw/main/material/day2_afternoon.pdf)
- [Day 3 Morning slides](https://gitlab.com/wur/linkeddata/-/raw/main/material/day3_morning.pdf)
- [Day 3 Afternoon slides](https://gitlab.com/wur/linkeddata/-/raw/main/material/day3_afternoon.pdf)
- [Day 4 Morning slides](https://gitlab.com/wur/linkeddata/-/raw/main/material/day4_morning.pdf)
- [Day 4 Bonus slides](https://gitlab.com/wur/linkeddata/-/raw/main/material/day4_afternoon.pdf)
- [Day 5 FAIRDS](https://gitlab.com/wur/linkeddata/-/raw/main/material/day5_fairds.pdf)
- [Day 5 Video](https://gitlab.com/wur/linkeddata/-/raw/main/material/ASPAR_KR_M1_24022023.mp4)