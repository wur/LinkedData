# Practical 2 - RDFS Hands-on[^1]

1\. In this exercise we are going to extend an existing RDFS vocabulary.
While later we are going to use graphical editors creating
ontologies, for now we will use our text editor for writing the turtles
and using RDF grapher[^2] for visualization.

a. Our starting point is the file in the previous exercise,
[pub-vocabulary-rdfs-simple.ttl](https://gitlab.com/wur/linkeddata/-/raw/main/material/pub-vocabulary-rdfs-simple.ttl). Open it in a text editor. At any
point, you can use RDF grapher to visualize your vocabulary by copy
pasting the text content into its input area.

b. Now that we have seen concept hierarchies in RDFS, inspect the given
vocabulary (the diagram or the text). Focus particularly on the
hierarchy of the classes in this vocabulary. The hierarchy implies a
series of 'IS-A' relationships among classes. For instance, a *Person*
is an *Agent*, while an *Agent* is a *Concept*. Similarly, inspect the
hierarchy of the properties.

c. Add a new class *Actor* in the hierarchy under Person i.e. as a
subclass of Person. by adding a new turtle representing this class. Make
sure to add additional metadata as you find useful, such as labels and
comments.

d. Next, we want to add a new property *placeOfBirth* representing the
place of birth of a *Person* with a suitable *Location*. Add this as a
new RDFs property, with the proper restrictions on domain and range.

e. We want to further add the property *dateOfBirth* for a *Person*,
however this time we just want to represent the date as a simple string
(e.g. in the format \"DD-MM-YYYY\"). Add this as a new property, also
with the proper domain and range.

f. So far we have added classes and properties, which make up the
vocabulary part. We can also add individuals (i.e. instances of classes)
and relationships about those instances. Add a new instance of the class
*Location* named *California*. Make sure that this is indeed a of the
type *Location*, by having it explicitly in the RDF statement.

g. Add a new instance *Bryan_Cranston* of the class *Actor* (i.e.
setting its type). Additionally, we want to specify his date and place
of birth. Set those to *California* (the particular instance we created
in the previous step) and \"07-03-1957\".

h. We are finished entering information about our vocabulary and
instances. Visualize the resulting resulting file, containing both the
base vocabulary and the extension statements in RDFGrapher.

````{admonition} Reveal the answer
:class: dropdown

Check the file [pub-vocabulary-rdfs-solution.ttl](https://gitlab.com/wur/linkeddata/-/raw/main/material/pub-vocabulary-rdfs-solution.ttl).


````


2\. In this second part, we are going to learn how to create a
vocabulary from scratch with given specifications.
This is the information we are going to create a vocabulary for:

| Main Concepts    | <div style="width:70px">Modifiers</div> | Data Properties | Object Properties |
| -------- | ------- | -------- | ------- |
| NamedEntity | Genre  | name for NamedEntity | has_genre for Movie -> Genre |
| -Person | -Action | duration for Movie | acted_in for Artist -> Movie |
| --Director | -Adventure | gross for Movie  | directed for Director -> Movie |
| --Artist | -Thriller | imdb_score for Movie | co-star_with for Artist -> Artist |
| ---Actor | -Comedy | imdb_link for Movie |  |
| ---Actress | -Drama |  |  |
| -Work |  |  |  |
| --Movie |  |  |  |

a. Open the file [movies-rdfs-empty.ttl](https://gitlab.com/wur/linkeddata/-/raw/main/material/movies-rdfs-empty.ttl) as a starting point to write
the RDFS statements for our vocabulary.

b. The columns in the above table named "Main Concepts" and "Modifiers"
specifies what classes we need in our vocabulary. Create new class
hierarchies (as we did in the previous practical) for the main concepts
and modifiers. In the table, single dashes imply a subclass, the longer
dashes imply a subclass of the subclass. For instance, *Director* is a
subclass of *Person*, which is itself a subclass of *NamedEntity*.

c. Check the data properties column. For each item, create an RDFS
Property. The domain of each data property is already given in the
table, for instance *name* for the domain *NamedEntity*. Decide on a
suitable data type (e.g. xsd:string) for the range of each data property
as well.

d. Check the object properties column. For each item, create an RDFS
property. The domain and range of each object property is already given
in the table, for instance *has_genre* for the domain *Movie* and the
range *Genre*.

e. Create some instances of movies with directors, along with actors and
genres and properties (e.g. pick two of your favourite movies and search
in IMDB). Add the information in the form of turtles.

f. What data properties can a Movie instance have? What role does the
class hierarchy play here?

g. Think about the way we have modelled genres, with genre types as
subclasses. Does it make sense to have genre types of subclasses? Should
individual genre types be concepts or instances? Change the vocabulary
so that Genre is a concept and the individual types (Action, Adventure)
are instances.

h. Can movies have no genres according to our model? Try to create a
movie instance with no genre.

i. Can movies have multiple genres according to our model? Try to create
a movie instance with two genres.

````{admonition} Reveal the answer
:class: dropdown
Check the file [movies-rdfs-solution.ttl](https://gitlab.com/wur/linkeddata/-/raw/main/material/movies-rdfs-solution.ttl).
````

3\. [OPTIONAL] Start thinking about your own dataset you'll use in this course. What classes, data properties and object properties would it have? What would the types for data properties and domain/range for object properties be? What about class hierarchies? 

[^1]: For questions email onder.babur@wur.nl

[^2]: <https://www.ldf.fi/service/rdf-grapher>
