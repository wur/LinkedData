# Practical 1 - RDF Hands-on[^1]

## Material

In the first part of this practical, we are going to practice writing
and visualizing RDFs as we saw in class. We are going to the following
online services: RDF grapher[^2] and RDF converter[^3].

1\. In this exercise, we will write and visualize simple RDFs in
different notations.

a. Open RDF grapher in your browser. It comes with a small example in
the text box as given below, consisting of a namespace and a single RDF
statement. Click on the button 'Visualize' and observe the input
visualized as a graph. Make sure you have the default settings for input
format as 'Turtle' and output format as 'PNG' as in figure
below.

![](../images/RDFVisualizerStartup.png)
*RDF Grapher user interface*


```{admonition} Reveal the answer
:class: dropdown

![](../images/rdf-grapher-example.png)
```

b. Re-write the same example using (1) N-Triples as seen in class, and
(2) using RDF-XML. For RDF-XML you can use the online tutorial by linked
data tools[^4], or if needed the full reference[^5]. For each notation,
make sure you choose the right input format and visualize them. Hint: you can skip the XML part if it is too time-consuming, and instead check the answer provided below! 

````{admonition} Reveal the answer
:class: dropdown

**N-triples notation**

```
    <http://example.com/s> <http://example.com/p> <http://example.com/o> .

```

**RDF-XML notation**
	
```xml    
    <?xml version="1.0" encoding="utf-8" ?>
    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
             xmlns:ex="http://example.com/">

      <rdf:Description rdf:about="http://example.com/s">
        <ex:p rdf:resource="http://example.com/o"/>
      </rdf:Description>

    </rdf:RDF>
```
````

c. Write the full set of RDF statements for the graph in figure
below. Don't forget to identify and write all the namespaces (you can
write arbitrary URIs for the namespaces for now). Enter all the
statements in RDF grapher and visualize it. Check if you get the right
graph, compared to the original figure.

![](../images/WalterWhite.png)
*RDF graph for Walter White*

````{admonition} Reveal the answer
:class: dropdown
**Turtle notation**
```

    @prefix dbp: <http://dbpedia.org/property/> .
    @prefix dbo: <http://dbpedia.org/ontology/> .
    @prefix dbpp: <http://dbpedia.org/property/> .
    @prefix ex: <http://example.com/> .
    @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
    @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .

    dbp:Walter_White_Breaking_Bad 
       dbpp:portrayer dbp:Brian_Cranston ;
       dbo:series dbp:Breaking_Bad ; 
       dbo:spouse dbp:Skyler_White ; 
       rdfs:label "Walter White"@en ; 
       dbo:alias "Heisenberg"@en . 

    dbp:Skyler_White dbpp:portrayer dbp:Anna_Gunn.

    dbp:Walter_White_Breaking_Bad ex:affiliation _:1 ; 
       ex:affiliation _:2 . 

    _:1 ex:affiliationPlace ex:Winnebago ;
       rdf:type ex:DrugDealer .

    _:2 ex:affiliationPlace ex:J_P_Wynne_High_School ;
       rdf:type ex:Teacher .
```
![](../images/walter-solution.png)
````

d. You can convert your RDF statements to another format, either
manually or through RDF converter. Either way, convert them to (1)
N-Triples and (2) RDF/XML and inspect the results.

````{admonition} Reveal the answer
:class: dropdown

**N-triples**

```{code}
	<http://dbpedia.org/property/Walter_White_Breaking_Bad> <http://dbpedia.org/property/portrayer> <http://dbpedia.org/property/Brian_Cranston> .
	<http://dbpedia.org/property/Walter_White_Breaking_Bad> <http://dbpedia.org/ontology/series> <http://dbpedia.org/property/Breaking_Bad> .
	<http://dbpedia.org/property/Walter_White_Breaking_Bad> <http://dbpedia.org/ontology/spouse> <http://dbpedia.org/property/Skyler_White> .
	<http://dbpedia.org/property/Walter_White_Breaking_Bad> <http://www.w3.org/2000/01/rdf-schema#label> "Walter White"@en .
	<http://dbpedia.org/property/Walter_White_Breaking_Bad> <http://dbpedia.org/ontology/alias> "Heisenberg"@en .
	<http://dbpedia.org/property/Walter_White_Breaking_Bad> <http://example.com/affiliation> _:genid1 .
	<http://dbpedia.org/property/Walter_White_Breaking_Bad> <http://example.com/affiliation> _:genid2 .
	<http://dbpedia.org/property/Skyler_White> <http://dbpedia.org/property/portrayer> <http://dbpedia.org/property/Anna_Gunn> .
	_:genid1 <http://example.com/affiliationPlace> <http://example.com/Winnebago> .
	_:genid1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://example.com/DrugDealer> .
	_:genid2 <http://example.com/affiliationPlace> <http://example.com/J_P_Wynne_High_School> .
	_:genid2 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://example.com/Teacher> .
```

**RDF-XML**

```{code}
	<?xml version="1.0" encoding="utf-8" ?>
	<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	         xmlns:ns0="http://dbpedia.org/property/"
	         xmlns:ns1="http://dbpedia.org/ontology/"
	         xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
	         xmlns:ns2="http://example.com/">
	
	  <rdf:Description rdf:about="http://dbpedia.org/property/Walter_White_Breaking_Bad">
	    <ns0:portrayer rdf:resource="http://dbpedia.org/property/Brian_Cranston"/>
	    <ns1:series rdf:resource="http://dbpedia.org/property/Breaking_Bad"/>
	    <ns1:spouse>
	      <rdf:Description rdf:about="http://dbpedia.org/property/Skyler_White">
	        <ns0:portrayer rdf:resource="http://dbpedia.org/property/Anna_Gunn"/>
	      </rdf:Description>
	    </ns1:spouse>
	
	    <rdfs:label xml:lang="en">Walter White</rdfs:label>
	    <ns1:alias xml:lang="en">Heisenberg</ns1:alias>
	    <ns2:affiliation>
	      <ns2:DrugDealer>
	        <ns2:affiliationPlace rdf:resource="http://example.com/Winnebago"/>
	      </ns2:DrugDealer>
	    </ns2:affiliation>
	
	    <ns2:affiliation>
	      <ns2:Teacher>
	        <ns2:affiliationPlace rdf:resource="http://example.com/J_P_Wynne_High_School"/>
	      </ns2:Teacher>
	    </ns2:affiliation>
	
	  </rdf:Description>
	
	</rdf:RDF>
```
````

e. What are the differences among these three textual notations, namely
Turtle, N-Triples and RDF/XML?

````{admonition} Reveal the answer
:class: dropdown
Conciseness, efficiency when humans reading vs. machines reading.
````

f. You are provided a set of RDF statements representing a (small part)
taxonomy for publishing information. Open the file [pub-vocabulary-rdfs-simple.ttl](https://gitlab.com/wur/linkeddata/-/raw/main/material/pub-vocabulary-rdfs-simple.ttl) in a text editor. Copy
and paste all the content into RDF Grapher and visualize them.

````{admonition} Reveal the answer
:class: dropdown

![](../images/pub-initial.png)
````

g. Some nodes on the graph are represented by ovals, while others being
shown as rectangles. What is the main difference between these two types
of nodes?

````{admonition} Reveal the answer
:class: dropdown
Ovals represent resources (with URIs), while rectangles represent
literals.
````


h. In some of the rectangles, we see additional information such as
'Language' and 'Datatype'. What do you think these might indicate?

````{admonition} Reveal the answer
:class: dropdown
Language is a special tag for string literals, indicating what language
they are in. Datatype in turn indicates what type of data (e.g. string,
integer, decimal number, date, so on) the literal represents.
````

2\. [OPTIONAL] In this part, we will investigate and discuss some key concepts in
Linked data, Knowledge Graphs and FAIR Data.

a. We have seen the four main categories in FAIR data in the lecture. Go to <https://www.go-fair.org/fair-principles/>, and read about the sub-categories and the FAIR-ification process. Consider a dataset in your own practice/field. Which of the items apply to your dataset? 

b. Find 5 companies, which are working with Knowledge Graphs and were
not mentioned in the lecture. Shortly describe them and their projects.

c. Consider a typical research project you have done yourself or
observed in your domain. Explain what you have observed in terms of each
step of the research data lifecycle as we have seen in the morning. In
that project, did you experience any missing step (e.g. data not
persisted) or any room for improvement?

d. As we discussed briefly in the lecture, there are a lot of widely
used vocabularies and ontologies for Linked Data. Go to
<https://lov.linkeddata.es/dataset/lov/> and investigate what some of
these already-curated vocabularies contain. Is there any vocabulary
which you could use in your research domain?

[^1]: For questions email onder.babur@wur.nl

[^2]: <https://www.ldf.fi/service/rdf-grapher>

[^3]: <https://www.easyrdf.org/converter>

[^4]: <http://www.linkeddatatools.com/introducing-rdf-part-2>

[^5]: <https://www.w3.org/TR/rdf-syntax-grammar/>
