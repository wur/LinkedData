# Knowledge graphs in the life sciences

* 13-17 March 2023
* Wageningen, the Netherlands
* Organizer: BioSB research school

**Course coordinator**

Jasper Koehorst, Wageningen University 
(location WUR-campus)

**Lecturers**
* Önder Babur (Wageningen University & Research)
* Anna Fensel (Wageningen University & Research)
* Jasper Koehorst (Wageningen University & Research)
Course credits

1.5 ECTS for following the course, 3 ECTS when successfully completing a final assignment

## Course overview
The term “FAIR Linked Data” refers to a set of best practices for publishing, interlinking and managing structured data on the Web. Semantic Web is an extension of the World Wide Web through recommendations set by the World Wide Web Consortium (W3C). Linked Data practices are implemented with Semantic Web technologies through which computers can query data and enable humans and machines to work in cooperation through a formalised shared meaning. Using knowledge representation formats (such as RDF, RDFS and OWL), ontologies and vocabularies to link data together, inferences can be drawn that show missing links within a given dataset and across datasets. Furthermore, used with Linked Data, SPARQL allows us to query the available information globally. Linked Data technologies enable researchers and industry to share data in ways that are Findable, Accessible, Interoperable, and Reusable (FAIR). As the Web evolves from a web of documents to a Web of Data, Linked Data technologies, such as knowledge graphs, are already broadly in use in industry and research. The first part of the course introduces you to the knowledge, theory, and skills needed to work with Linked Data. We will then focus on research and industrial data applications and needs that are increasingly in demand.

**The learning outcomes are as follows:**

* explain Linked Data technologies (Internet, Semantic Web, ontologies, graph databases), standards and recommendations (as URI, XML, RDF, OWL, SPARQL), and their use in forming a Web of Data; 
* use Linked Data technologies for retrieving information in the Semantic Web (i.e. use of SPARQL); 
* use of existing vocabularies for annotating data and endpoints for finding data for a particular domain; 
* understand the FAIR principles and how to apply it to a research project;
* convert research metadata into FAIR Linked Datasets.

**Daily schedule:**

09:00 – 12:00 Morning session
12:00 – 13:00 Lunch (included)
13:00 – 16:00 Afternoon session

**Locations and topics:**

13, 14, 15 March: Forum, B0773

**Monday:** Introduction into linked data technologies

**Tuesday:** Understanding and developing vocabularies.

**Wednesday:** Using linked data technologies for data retrieval.

16 March, Forum, B0767

**Thursday:** Data transformation, how to generate linked data
Thursday afternoon: BYOD / Explain

17 March, Forum, B0106

**Friday:** FAIR Data in practice


```{tableofcontents}
```

<!-- ## The semantic web

The semantic web is an extension of the World Wide Web which promotes common data formats and exchange protocols on the web, most fundamentally written in Resource Discription Framework (RDF). 

There is a great amount of data available on the World Wide Web, of which most buried in text, like scientific papers on Pubmed.
To demonstrate; each year about 1.2 million scientific papers are published and 100 new databases emerge.

![](./images/papers.jpg)

Data retrieval from text is can be fairly challenging, unless a very powerfull text mining algorithm is used.
But, even more data is becoming available such as databases like Protein Data Bank (PDB), European Bioinformatics Institute (EBI), MetaCyc and hundreds more.

The production of this excessive amount of data has lead to emergence of the field of semantics, which attempts to create structured and accessible data.

## Introduction to RDF and Ontologies

The field of semantics not only aims to generate structured and accessible data, but also to tries to create a web of data that can be processed by machines — meaning, of which the meaning is machine-readable. This is also called meta-data, literally meaning data about data.

In order to store the information that is machine-readable, the RDF data model is used which promotes communication between machines. 
The RDF data model makes statements about (web)resources in expressions of the form subject-predicate-object, better known as **triples**. 
The *subject* denotes the resource, and the *predicate* denotes traits or aspects of the resource, and expresses a relationship between the *subject* and the *object*. The *object* is another resource that is linked to the *subject*. The semantic web can therefore also be called **triple-stores** or **linked data**. 

|![](./images/rdf-turtle-example.png)|
|:--:| 
|*example of RDF language*|

The relations between these triples or linked data are already pre-defined by making use of ontologies. An ontology pre-defines the relationships between linked data or triples even before RDF is used. Ontologies are used to classify the terms (subject/object) that can be used in a particular application, characterize possible relationships, and define possible constraints on using those terms. In practice, ontologies can be very complex (with several thousands of terms) or very simple (describing one or two concepts only). The Description Framework (RDF) is subsequently created on the basis of the ontology that is defined. 

The role of ontologies on the Semantic Web are to help data integration when, for example, ambiguities may exist on the terms used in the different data sets, or when a bit of extra knowledge may lead to the discovery of new relationships. <br>
Consider, for example, the application of ontologies in the field of health care. Medical professionals use them to represent knowledge about symptoms, diseases, and treatments. Pharmaceutical companies use them to represent information about drugs, dosages, and allergies. Combining this knowledge from the medical and pharmaceutical communities with patient data enables a whole range of intelligent applications such as decision support tools that search for possible treatments; systems that monitor drug efficacy and possible side effects; and tools that support epidemiological research.

Another type of example is to use ontologies to organize knowledge. Libraries, museums, newspapers, government portals, enterprises, social networking applications, and other communities that manage large collections of books, historical artifacts, news reports, business glossaries, blog entries, and other items can now use ontologies, using standard formalisms, to leverage the power of linked data.

It depends on the application how complex ontologies become. To satisfy different complexity needs of ontologies, the World Wibe Web Consortium (W3C) offers a large palette of techniques to describe and define different forms of ontologies in a standard format. These include RDF and RDF Schemas, Simple Knowledge Organization System (SKOS), Web Ontology Language (OWL), and the Rule Interchange Format (RIF). The choice among these different technologies depend on the complexity and rigor required by a specific application.

![](./images/ontology-taxonomy.png)

*An ontology is a formal specification of a shared conceptualization (Tom Gruber, 1993)*

In the best case scenario the semantic web is developed according to the FAIR principles.

## The FAIR Data principles

There is an urgent need to improve the infrastructure supporting the reuse of data. Therefore, a concise and measureable set of principles were defined that we refer to as the FAIR Data Principles. The intent is that these may act as a guideline for those wishing to enhance the reusability of their data. The FAIR Principles put specific emphasis on enhancing the ability of machines to automatically find and use the data, in addition to supporting its reuse by individuals. 

### Findable

Findable comprises two major attributes: the data set be identifed by a URL/URI/IRI/PID, so that it can be unambiguously located by a machine and the data be described by sufficient metadata so that it an be discovered by a human. Identifiers are linked to this rich metadata so that a human can verify that the dataset is correctly resolved. Finally, the data must be registered or indexed so that it can be found. The more machine-readable metadata that a data resource provides for its contents, the more likely a web search through a search engine like Google will find them.

### Accessible

Accessible provides requirements for ensuring that third parties can access the data by specifying that the URL/URI/IRI/PID is resolvable and that the access protocol is standard, e.g., http, and open. The access protocol should allow for authentiation and authorization to protect sensitive data. Finally, a critical requirement to ensure the integrity of links across the web is that the metadata persist, even if the data they describe have been removed for some reason. In this way, a basic description of the data can still be found at the same URL/URI/IRI/PID, much like descriptive metadata of out of print books can still be found. Ideally, the page describing the metadata includes a statement about when and why the data were removed (Starr et al., 2015).

### Interoperable

Interoperable outlines requirements for ensuring that data across databases can be combined with minimal effort. Note that this principle specifically calls for the use of formal ontologies widely used in a community that are themselves FAIR, i.e., have a persistent identifier and access protocol.

### Reusable

Reusable also emphasizes rich metadata, but in this case so that the dataset can be sufficiently understood by a human so that it can be reused appropriately. Many communities have published minimal information models that provide the minimum set of attributes required for proper interpretation of a given data set, e.g., MIAME. Reusable also specifies that a data licence, preferable in a machine-readable form, accompanies the data specifying how it may be re-used, e.g., a CC-BY license specifies that the data may be re-used without restriction, but attribution of the source must accompany the reuse. And, of course, re-usable emphasizes the use of community-accepted standards for metadata and for data themselves. A major goal of ReproNIM is to help define these standards for neuroimaging.

![](./images/fair-data-principles.jpg)

We will further elaborate about how to make information in a linked-data framework accessible in the following chapter. -->