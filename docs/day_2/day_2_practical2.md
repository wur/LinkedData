---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---
   
# OWL and Basic Ontology Engineering

## Exercises

**Exercise 1 (OWL reasoning)**

*See below for an example on using Protegé*

Provided with the ontology about people, things and animals show that the following classes and instance inferences are true:
- Bus Drivers are Drivers.
- Cat Owners like Cats.
- Walt is an animal lover.
- Tom is a Cat.

![](../images/OWL-reasoning.png)

**Exercise 2 (OWL modeling)**

Design an OWL DL ontology (using Protégé[^1]) which models the domain of bird observations (basing on the dataset provided by us). Use class axioms and relations to other properties, e.g. owl:inverseOf - employ at least 3 OWL-specific constructions. 

- [Observations of the Bar-tailed Godwit](https://gitlab.com/wur/linkeddata/-/raw/main/material/observations-301836.csv.gz)

**Exercise 3 (OWL inference)**

Based on the OWL ontology developed in Exercise 2 infer other possible statements (at least two statements).

### Protegé 

> We will mostly use the properties from schema.org or self-invented terms. When working with a new dataset it is adviced to re-use existing ontologies when possible.

To create a new ontology in protégé start with an empty ontology. 

-	Save and a popup should appear to specify the format 
  - Choose RDF/XML syntax
  
#### Classes 

-	To create a new class (e.g. http://schema.org/GeoCoordinates) click on the first icon under the Class hierarchy - 	Give this class a name 
As you can see it will generate a very long URL, you can change its default behavior under the New entity options or right click on the created class name and Change IRI (show full IRI) 
-	When you already have complete URLS such as https://schema.org/GeoCoordinates you can use that and it will not change the IRI. 

![](../images/protege-classes.png) 

-	The corrected URL can be found just below annotations

![](../images/protege-annotations.png)

### Predicates 
-	Object properties connect two individuals (a subject and object) via a predicate functioning as the verb phrase in a sentence

![](../images/protege-objects.png)

-	Specify the URL of this property (predicate) 

![](../images/protege-object-url.png)

-	When defining complete URLs it will automatically use that.

*Note:* If you want you can create all classes, the hierarchy and predicates in an ontology depending on the amount of time you have and how familiar you are with the dataset. You can always update the ontology at a later stage or create classess and predicates on the fly.

Make sure you save the file as an `rdf/xml` file as this is the default format for `.owl`.

[^1]: https://protege.stanford.edu 