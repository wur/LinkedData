---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

   
# Ontologies and Basic Ontology Engineering

## Exercises

**Exercise 1 (Ontologies I)**

Explain in your own words what an ontology is. Provide 3 examples of different ontologies, which were not mentioned in the lecture, and shortly describe them. (min 0.5 pages, max 1 page).

**Exercise 2 (Ontologies II)**

Explain what the differences are:

1.	between an ontology and a taxonomy
2.	between an ontology and a folksonomy

**Exercise 3 (Ontologies III)**

List at least two technical tasks for which you think ontologies are most useful.

**Exercise 4 (Basic Ontology Engineering I)**

Explain what ontology engineering is. Explain what for it is needed.

**Exercise 5 (Basic Ontology Engineering II)**

Give an example of a domain where you are not an expert.
Imagine that your task is to develop ontologies for this domain. How would you approach this task?

Explain what the difference between top-down and bottom-up ontology construction is.

**Exercise 6 (Basic Ontology Engineering III)**

Take a look at the SKOS vocabulary: https://www.w3.org/2004/02/skos/, and explain what it is useful for.

Find two different ontologies that describe the same domain with different (but similar) terms.

The ontologies should not be the ones used in the SKOS specification examples.

Model in RDF at least 4 relations for the terms of the two different ontologies chosen by you (how the terms of the two ontologies are related). For this RDF modelling, use at least 3 different constructions from SKOS that describe the relations between the terms.

**Exercise 7 (Basic Ontology Engineering IV)**

Validate your RDF resulting from the Exercise 6 with an RDF validator of your choice (e.g. https://www.w3.org/RDF/Validator/ but you can also choose another one), and correct any possible mistakes. 
Have you encountered any issues, and if yes, which ones?

**Exercise 8 (Ontology Engineering Methodologies)**

Choose two different ontology engineering methodologies and briefly explain in your own words their characteristics.